from bs4 import BeautifulSoup
import requests
import pandas as pd

car_list = {}
base_url = 'https://www.carsensor.net'
car_type_list = 'https://www.carsensor.net/usedcar/index.html?STID=CS210610&AR=0&BRDC=&CARC={0}&NINTEI=&CSHOSHO='

shashu_page_list = []

def get_html_for_url(url):
    req = requests.get(url)
    req.encoding = req.apparent_encoding
    soup = BeautifulSoup(req.text, 'lxml')
    return soup

def main():
    soup = get_html_for_url(base_url)
   
    car_link_list = []

    for link in soup.find_all('a', class_='makerNav__anchor--head'):
        car_link_list.append(link['href'])

    for all_car_link in car_link_list:
        soup = get_html_for_url(base_url + all_car_link)

        for car_type in soup.find_all('li', class_='makerAll__list'):
            a_tag = car_type.find('a')
            car_list[a_tag.text] = a_tag['href']

    for car in car_list.keys():
        soup = get_html_for_url(base_url + car_list[car])
        
        for car in soup.find_all('div', class_='shashuList__category__item'):
            c = car.find('input')
            if c is not None:
                shahu_value = c['value']
                shashu_page_list.append(car_type_list.format(shahu_value))
    
    df = pd.DataFrame(shashu_page_list)
    df.to_csv('***your Path****.csv', header=False)


if __name__ == "__main__":
    main()





